$( document).ready(function() {
   
// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

const $aElement=$('<a>');
$aElement.text('Buy Now!');
$aElement.attr('id','cta');
$('main').append($aElement);

// Access (read) the data-color attribute of the <img>,
// log to the console

console.log($('img').data("color"));

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"

console.log($("li").eq(2).addClass("highlight"));

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

$('p').last().remove(); 

// Create a listener on the "Buy Now!" link that responds to a click event.
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"

$('#cta').click(function(e){

    $(this).remove();
    const $pNew=$('<p>');
    $pNew.text('Added to cart');
    $('main').append($pNew);
});




}); 