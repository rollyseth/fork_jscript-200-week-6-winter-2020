// Do not change
document.getElementById('cat').addEventListener('click', () => {
  alert('meow!');
});

// When clicked, "More info" link should alert "Here's some info"
// instead of going to a new webpage


$('#more-info').click(function(e){
  e.preventDefault();
  alert('Here\'s some info');
});

// document.getElementById('more-info').addEventListener('click', (e) => {
//   e.preventDefault();
//   alert('here\'s some info');
// });

// When the bark button is clicked, should alert "Bow wow!"
// Should *not* alert "meow"

$('#dog').click(function(e){
  alert('Bow wow!');
  e.stopPropagation(); 
});
