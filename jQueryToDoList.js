$( document).ready(function() {
/**
 * Toggles "done" class on <li> element
 */


$("li").click(function(e){
  e.preventDefault(); 
 $(this).toggleClass('done');
 console.log(this);

});


/**
 * Delete element when delete link clicked
 */

$('.delete').click(function(e){

 //const li=this.parentElement;
 //const ul = li.parentElement;
 const $li =$(this).parent() ;
 const $ul =$($li).parent()  ;

 e.stopPropagation();

 $($li).fadeOut("slow", function() { $($li).remove(); }); //fading out 
 console.log(this);

 
});


/**
 * Adds new list item to <ul>
 */
const addListItem = function(e) {
  
  const $text = $('input').val();
  const $newLi=$('<li>');

  //creating new li, appending span and a tags in that and adding that to the ul
  const $newSpan=$('<span>');
  $newSpan.text($text);
  $newLi.append($newSpan);
  const $a=$('<a>');
  $a.addClass('delete');
  $newLi.append($a);
  $a.text('Delete');
    console.log($a);
  $('.today-list').append($newLi);
  console.log($newLi);

  //adding listeners to newly created li elements 
  $($newLi).on('click', function(e) {
    e.stopPropagation(); 
    $(this).toggleClass('done');
    console.log(this);
  });

  $('.delete').click(function(e){

    //const li=this.parentElement;
    //const ul = li.parentElement;
   
    const $li =$(this).parent() ;
    const $ul =$($li).parent()  ;
    e.stopPropagation();
   
    $($li).fadeOut("slow", function() { $($li).remove(); }); //fading out 
    console.log(this);
   });

};


// add listener for add
$('.add-item').click(function(e){ 
  
  addListItem(); 
  e.stopPropagation();
  console.log(this);
 });

}); 
